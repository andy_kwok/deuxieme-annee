from datetime import datetime

def premier(nb):
    n = 2
    while n<=nb//2 and nb%n != 0:
        n += 1

    if n>nb//2 :
        return True
    else :
        return False

def classique(nbmax):
    premiers = []
    for nb in range(1, nbmax+1):
        if premier(nb) : premiers += [nb]
    return premiers

def cribble(nbmax):
    premiers = [True] * (nbmax+1)

    for nb in range(2, (nbmax+1)//2):
        mult=2
        while mult*nb <= nbmax:
            premiers[mult*nb] = False
            mult += 1

    resultats = []
    for nb in range(1, nbmax+1):
        if premiers[nb] : resultats += [nb]
    return resultats



nbmax = int(input("Valeur maximale : "))
while nbmax <= 0 :
    print("Valeur incorrecte !")
    nbmax = int(input("Entrez un entier strictement positif :"))



print("\nDébut de l'algorithme classique...")
debut = datetime.now()
classique(nbmax)
duree_classique = datetime.now() - debut

print("\nDébut du cribble d'Eratosthène...")
debut = datetime.now()
res = cribble(nbmax)
duree_cribble = datetime.now() - debut

print("\nNombres premiers inférieurs à", nbmax, ':')
print(res)
print("\nDurée de l'algo classique :", duree_classique)
print("Durée du cribble d'Eratosthène :", duree_cribble)


