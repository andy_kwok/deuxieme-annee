import random
from datetime import datetime

n = int(input("Nombre d'entiers : "))

liste = [0]*n
for i in range(n):
    liste[i] = random.randint(0,100)

debut = datetime.now()
print("\n-- Afficher la liste élément par élement --")
for i in range(n):
    print(liste[i], end=' ')
duree1 = datetime.now() - debut


debut = datetime.now()
print("\n-- Afficher la liste --")
print(liste)
duree2 = datetime.now() - debut


print("\nDurée du traitement 1 :", duree1)
print("Durée du traitement 2 :", duree2)


