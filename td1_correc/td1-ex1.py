#correction 1


n = int(input("n = "))

somme = 0
for i in range(n):
    somme += (2*i+1)**2

print("Somme des carrés des", n, "premiers impairs :", somme)    

#correction 2    


n = int(input("n = "))

resultat = 0
for k in range(1, n * 2, 2):
    resultat = resultat + (k * k)
print(resultat)  




