import random
import module_ex3
from datetime import datetime

n = int(input("Nombre d'entiers : "))

liste = [0]*n
for i in range(n):
    liste[i] = random.randint(0,100)

print("\n-- Afficher la liste élément par élement --")
debut = datetime.now()
module_ex3.print_list2(liste)
duree1 = datetime.now() - debut


print("\n-- Afficher la liste --")
debut = datetime.now()
print(liste)
duree2 = datetime.now() - debut


print("\nDurée du traitement 1 :", duree1)
print("Durée du traitement 2 :", duree2)


