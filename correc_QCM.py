"""
Si s = [-10, 40, 15, -20, 30], qu'affiche à l'écran la variable t ?

t = [abs(x) for x in s]
print(t)


Si s = [10, 40, -15, -20, 30], qu'affiche à l'écran la variable t ?

t = [x / 2 for x in s if x < 0]
print(t)


Qu'affiche à l'écran la variable t ?

t = [i%2 for i in range(11, 21)]
print(t)


"""