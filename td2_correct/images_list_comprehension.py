'''
   
    1. Créer la liste le des entiers de 1 à 20
    2. Créer la liste des éléments de le aux carré
    3. Créer la liste des éléments pairs de le
    4. Créer la liste des éléments inférieurs à 7 de le, mis à la puissance 4
    5. Créer une liste la de 50 entiers tirés aléatoirement dans [-100,100]
    6. Créer la liste des valeurs absolue des éléments de la
    7. Calculer la moyenne puis l’écart-type de la
'''

liste_1_20 = [i for i in range(1,21)]

#print(liste_1_20)

liste_le_carre = [i**2 for i in liste_1_20]

#print(liste_le_carre)

liste_pair = [el for el in liste_1_20 if el%2 ==0]

#print(liste_pair)

liste_inf_7 = [el for el in liste_1_20 if el < 7]

#print(liste_inf_7)

from random import randint

la = [randint(-100,100) for i in range(1,51)]

print(la)

la_abso = [el*-1 if el <0 else el for el in la]

print(la_abso)

moye = (sum(el for el in la))/len(la)

print("la moyenne est",moye)

#somme (1/n(xi-moy))**(1/2)

#https://stackoverflow.com/questions/15389768/standard-deviation-of-a-list

variance = sum([((x - moye) ** 2) for x in la]) / (len(la))
print("la variance est",variance)

ecart_type = variance ** 0.5

print("ecart type est :"ecart_type)

moye1 = sum(la)/len(la)

print("une autre façon de calculer la moyenne : ",moye1)