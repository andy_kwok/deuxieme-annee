# TP 4 Numpy - exercice 1

import numpy as np

def observer(ndarr):
	ndarr = np.array(ndarr)
	print(ndarr)
	print("- Type de données :", ndarr.dtype.name)
	print("- Nb de dimensions :", ndarr.ndim)
	print("- Forme :", ndarr.shape)
	print("- Nb d'éléments :", ndarr.size)
	
	
#a) un ndarray créé à partir de la liste suivante : [1, 4, 5, 6, 8]
print("\na)")
a = np.array([1, 4, 5, 6, 8])
observer(a)

#b) un ndarray créé à partir de la liste suivante : [1.5, 4, 5, 6, 8]
print("\nb)")
b = np.array([1.5, 1, 1, 1, 1])
observer(b)

#c) un ndarray contenant la somme élément par élément des deux tableaux précédents
print("\nc)")
c = a + b
observer(c)

#d) un ndarray représentant une matrice (4x5) contenant des réels nuls 
print("\nd)")
d = np.zeros((4, 5))
observer(d)

#e) un ndarray à 3 dimensions de 4 éléments dans chaque dimension et ne contenant que des 1 
print("\ne)")
e = np.ones((4, 4, 4))
observer(e)

#f) un ndarray contenant les entiers de 10 à 33 et de forme (2, 3, 4)
print("\nf)")
f = np.arange(10,34).reshape(2,3,4)
observer(f)

#g) un ndarray créé par l'appel :  np.empty( (2,3) ) 
print("\ng)")
g = np.empty( (2,3) )
observer(g)


