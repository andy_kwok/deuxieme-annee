# TP 4 Numpy - exercice 2

import numpy as np


def exo2():
	#1/ Créer un tableau appelé tab de forme (4, 3, 4) et contenant des entiers aléatoires dans [1, 100] 
	tab = np.random.randint(1, 100, (4,3,4))
	print("\n1/ tab\n", tab)

	#2/ Créer et afficher un tableau de même forme que tab et contenant tous ses éléments au carré 
	carr = tab**2
	print("\n2/ Carré\n", carr)

	#3/ Créer et afficher le tableau contenant tous les éléments de tab supérieurs à 10
	sup10 = tab[tab>10]
	print("\n3/ Supérieurs à 10\n", sup10)


	#4/ Créer et afficher le tableau contenant tous les éléments de tab divisibles par 3 
	div3 = tab[tab%3==0]
	print("\n4/ Divisibles par 3\n", div3)

	#5/ Créer et afficher le tableau qui contient la somme cumulée des éléments de tab
	sc = np.cumsum(tab)
	print("\n5/ Somme cumulée\n", sc)

exo2()

