"""
Si s = [-10, 40, 15, -20, 30], qu'affiche à l'écran la variable t ?

t = [abs(x) for x in s]
print(t)


Si s = [10, 40, -15, -20, 30], qu'affiche à l'écran la variable t ?

t = [x / 2 for x in s if x < 0]
print(t)


Qu'affiche à l'écran la variable t ?

t = [i%2 for i in range(11, 21)]
print(t)


"""



# creer une fonction qui comporte 
# une variable de type liste, appelé Liste1, contenant des valeurs allant de 0 à 255
# remplacer la 30ème valeur de la liste, Liste1, par la valeur 'a'
# parcourir la liste via un enumerate et si la valeur dans la liste contient la string 'a', 
# retourner la position ainsi que la valeur 'a' donc 'a',30


# Ecrire une fonction qui crée un dictionnaire du type suivant : (x, x*x)
# exemple de dictionnaire (n = 5) : 
# Résultat attendu : {1: 1, 2: 4, 3: 9, 4: 16, 5: 25}


# réduire le nombre de ligne en utilisant un dictionnaire, 
# en utilisant un 'in',  en gardant le if et en utilisant le keys()  

def si():
	var_inp = int(input('taper votre choix, de 1 à 5 \n'))
	if var_inp == 1:
		print('toto a 1')
	elif var_inp == 2:
		print('toto a 2')
	elif var_inp == 3:
		print(f"toto a 3")
	elif var_inp == 4:
		print(f"toto a 4")
	elif var_inp == 5:
		print(f"toto a 5") 	



